# README #

#### - The 'master' branch contains the latest version of the code, including all completed tasks.

#### - The 'required_task' branch contains only the required tasks.

#### - The 'racecar' branch contains the additional modifications to implement a race car which moves at 2 spaces instead of the sedans 1.

#### - The 'download_veyo_website' branch contains the modifications to supply a passenger with a smart phone which is then used at each move to download the Veyo homepage.

#### - The 'factory' branch contains the modifications to apply a factory interface for registering and creating car types along with their factory functions.

#### Each branch builds upon the previous branch in terms of modifications, refactoring, and tests.