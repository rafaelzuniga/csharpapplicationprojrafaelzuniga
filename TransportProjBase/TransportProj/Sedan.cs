﻿using System;

namespace TransportProj
{
    public class Sedan : Car
    {
        public Sedan(int xPos, int yPos, City city, Passenger passenger)
            : base(xPos, yPos, city, passenger) { }

        public override int MaximumSpeed
        {
            get { return 1; }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"Sedan moved to x - {XPos} y - {YPos}");
        }
    }
}
