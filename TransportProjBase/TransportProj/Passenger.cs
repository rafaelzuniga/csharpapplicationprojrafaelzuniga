﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj
{
    public class Passenger
    {
        private readonly ISmartPhone _smartPhone;

        public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int CurrentXPos { get; private set; }
        public int CurrentYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public Car Car { get; private set; }
        public City City { get; private set; }

        /// <summary>
        /// Since we aren't actually processing the website visit we need a way to expose it so it's testable
        /// </summary>
        public int WebsiteVisitCount { get; private set; }

        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city, ISmartPhone smartPhone)
        {
            CurrentXPos = StartingXPos = startXPos;
            CurrentYPos = StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;

            if (smartPhone == null)
            {
                throw new ArgumentNullException(nameof(_smartPhone));
            }

            _smartPhone = smartPhone;
        }


        /// <summary>
        /// Boards the passenger onto the given car if not already in one; throws an exception if already in a car
        /// </summary>
        /// <param name="car">The car the passenger will board</param>
        public void GetInCar(Car car)
        {
            if (car == null)
            {
                throw new ArgumentNullException(nameof(car));
            }

            if (Car != null)
            {
                throw new InvalidOperationException("Passenger already in car");
            }

            Car = car;
            car.PickupPassenger(this);

            Console.WriteLine("Passenger got in car.");
        }

        /// <summary>
        /// Exits the car if the passenger is in one; throws an exception otherwise
        /// </summary>
        public void GetOutOfCar()
        {
            if (Car == null)
            {
                throw new InvalidOperationException("Passenger is not in car");
            }

            Car = null;

            Console.WriteLine("Passenger got out of car.");
        }

        /// <summary>
        /// Checks whether the passenger is at their destination
        /// </summary>
        /// <returns>Returns true if at their destination; false otherwise</returns>
        public bool IsAtDestination()
        {
            return CurrentXPos == DestinationXPos && CurrentYPos == DestinationYPos;
        }

        /// <summary>
        /// An action to be called on each movement of the passenger. Note that this was previously
        /// implmented as function passed to the car via 'Car.SetOnMoveAction'. The idea behind having
        /// it on the passenger itself is that we can later imeplement it as an interface, IPassenger,
        /// and support multiple passenger types and have a clear contract that's easier to test versus
        /// having an optional action passed to the car.
        /// </summary>
        /// <returns>A task instance which can be awaited</returns>
        public async Task OnMoveAction()
        {
            const string Url = "https://veyo.com/";

            CurrentXPos = Car.XPos;
            CurrentYPos = Car.YPos;

            try
            {
                await _smartPhone.VisitWebsite(Url);
                ++WebsiteVisitCount;
            }
            catch (HttpRequestException ex)
            {
                Console.WriteLine($"Unable to visit website: '{Url}'");
                Console.WriteLine($"Exception:\n{ex}");
            }
        }
    }
}
