﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace TransportProj
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                var rand = new Random();
                const int cityLength = 10;
                const int cityWidth = 10;

                var city = new City(cityLength, cityWidth);
                var car = city.AddCarToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1));
                var passenger = city.AddPassengerToCity(rand.Next(cityLength - 1), rand.Next(cityWidth - 1), rand.Next(cityLength - 1), rand.Next(cityWidth - 1));

                IDictionary<Coordinate, int> visitedCoordinates = new Dictionary<Coordinate, int>();

                Console.WriteLine($"Car start: {car.XPos}, {car.YPos}");
                Console.WriteLine($"Passenger start: {passenger.StartingXPos}, {passenger.StartingYPos}");
                Console.WriteLine($"Passenger destination: {passenger.DestinationXPos}, {passenger.DestinationYPos}\n");

                while (!passenger.IsAtDestination())
                {
                    var currentCoordinate = await Tick(car, passenger);
                    VisitCoordinate(currentCoordinate, visitedCoordinates);
                }

                PrintVisitedCoordinates(visitedCoordinates);

                Console.WriteLine("\nDone!");
                Console.ReadKey();
            }).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        /// <returns>A Coordinate representing the location of the Car after the move was made</returns>
        private static async Task<Coordinate> Tick(Car car, Passenger passenger)
        {
            if (!car.HasPassenger())
            {
                if (car.IsAtPosition(passenger.StartingXPos, passenger.StartingYPos))
                {
                    passenger.GetInCar(car);
                }
                else
                {
                    await car.MoveTowards(passenger.StartingXPos, passenger.StartingYPos);
                }
            }
            else
            {
                await car.MoveTowards(passenger.DestinationXPos, passenger.DestinationYPos);

                if (passenger.IsAtDestination())
                {
                    passenger.GetOutOfCar();
                }
            }

            return new Coordinate
            {
                XPos = car.XPos,
                YPos = car.YPos
            };
        }

        /// <summary>
        /// Visits a coordinate by adding it to the visitedCoordinates collection.
        /// </summary>
        /// <param name="coordinate">the coordinate to visit</param>
        /// <param name="visitedCoordinates">the collection of coordinates that were already visited</param>
        private static void VisitCoordinate(Coordinate coordinate, IDictionary<Coordinate, int> visitedCoordinates)
        {
            if (!visitedCoordinates.ContainsKey(coordinate))
            {
                visitedCoordinates[coordinate] = 1;
            }
            else
            {
                ++visitedCoordinates[coordinate];
            }

            // Using a dictionary, adding / incrementing coordinates that we have visited gives us a complexity of the underlying
            // hash implementation which in our case will be very close to O(1) (see: https://msdn.microsoft.com/en-us/library/xfhwa508.aspx)
        }

        /// <summary>
        /// Prints all the coordinates that were visited and the number of times each coordinate was visited to the Console.
        /// For example, if the coordinate (1, 1) was visited once and the Coordinate (1, 2) was visited twice, the output should be as follows:
        /// 
        /// Visited coordinates:
        /// (1, 1) - 1
        /// (1, 2) - 2
        /// 
        /// </summary>
        /// <param name="visitedCoordinates">The collection of coordinates that were visited</param>
        private static void PrintVisitedCoordinates(IEnumerable<KeyValuePair<Coordinate, int>> visitedCoordinates)
        {
            Console.WriteLine("Visited coordinates:");

            foreach (var coordinate in visitedCoordinates)
            {
                Console.WriteLine($"({coordinate.Key.XPos}, {coordinate.Key.YPos}) - {coordinate.Value}");
            }

            // Given that we have to iterate through each element once the complexity is O(n)
        }
    }
}
