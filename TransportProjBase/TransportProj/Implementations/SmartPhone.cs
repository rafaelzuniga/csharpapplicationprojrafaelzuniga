﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Implementations
{
    public class SmartPhone : ISmartPhone
    {
        private readonly HttpClient _httpClient;

        public SmartPhone()
        {
            _httpClient = new HttpClient();
        }

        public async Task VisitWebsite(string url)
        {
            if (string.IsNullOrWhiteSpace(url))
            {
                throw new ArgumentException(nameof(url));
            }

            var response = await _httpClient.GetAsync(url);

            response.EnsureSuccessStatusCode();

            Console.WriteLine($"Visited website at '{url}'");
        }
    }
}
