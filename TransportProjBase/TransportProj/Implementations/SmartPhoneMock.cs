﻿using System.Threading.Tasks;
using TransportProj.Interfaces;

namespace TransportProj.Implementations
{
    /// <summary>
    /// Simple mock class for the ISmartPhone interface; normally you'd want to use a mock
    /// framework like Moq rather than roll your own but in this case it's much simpler.
    /// </summary>
    public class SmartPhoneMock : ISmartPhone
    {
        public Task VisitWebsite(string url)
        {
            return Task.WhenAll();
        }
    }
}
