﻿using System;
using System.Threading.Tasks;

namespace TransportProj
{
    public abstract class Car
    {
        private int _currentSpeed;

        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }

        public int CurrentSpeed
        {
            get { return _currentSpeed; }

            protected set
            {
                if (value > MaximumSpeed)
                {
                    _currentSpeed = MaximumSpeed;
                }
                else if (value < 1)
                {
                    _currentSpeed = 1;
                }
                else
                {
                    _currentSpeed = value;
                }
            }
        }

        public abstract int MaximumSpeed { get; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            CurrentSpeed = MaximumSpeed;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine($"Car moved to x - {XPos} y - {YPos}");
        }

        /// <summary>
        /// Picks up a passenger if the car is empty; otherwise an exception is thrown if a passenger is currently onboard
        /// </summary>
        /// <param name="passenger">The passenger to pick up</param>
        public void PickupPassenger(Passenger passenger)
        {
            if (passenger == null)
            {
                throw new ArgumentNullException(nameof(passenger));
            }

            if (Passenger != null)
            {
                throw new InvalidOperationException("Car already has a passenger");
            }

            Passenger = passenger;
        }

        /// <summary>
        /// Determines whether or not the car currently has a passenger
        /// </summary>
        /// <returns>Returns true if a passenger is onboard; false otherwise</returns>
        public bool HasPassenger()
        {
            return Passenger != null;
        }

        /// <summary>
        /// Checks whether or not the car is at the given X/Y position
        /// </summary>
        /// <param name="xPos">The X value to check against</param>
        /// <param name="yPos">The Y value to check against</param>
        /// <returns>Returns true if at the given position; false otherwise</returns>
        public bool IsAtPosition(int xPos, int yPos)
        {
            return XPos == xPos && YPos == yPos;
        }

        /// <summary>
        /// Moves a single step towards the given X/Y value moving up/down/left/right as needed.
        /// This method will automatically take into account any speed reduction needed to not
        /// overshoot the target position. This method does not find the shortest path and follows
        /// the following rules:
        /// 
        ///   - Move along the X axis first until resolved
        ///   - Move along the Y axis next until resolved
        /// </summary>
        /// <param name="posX">The X position to move towards</param>
        /// <param name="posY">The Y position to move towards</param>
        public async Task MoveTowards(int posX, int posY)
        {
            if (XPos > posX)
            {
                if (XPos - CurrentSpeed < posX)
                {
                    CurrentSpeed = posX - (XPos - CurrentSpeed);
                }

                await MoveLeft();

                CurrentSpeed = MaximumSpeed;
            }
            else if (XPos < posX)
            {
                if (XPos + CurrentSpeed > posX)
                {
                    CurrentSpeed = (XPos + CurrentSpeed) - posX;
                }

                await MoveRight();

                CurrentSpeed = MaximumSpeed;
            }
            else if (YPos > posY)
            {
                if (YPos - CurrentSpeed < posY)
                {
                    CurrentSpeed = posY - (YPos - CurrentSpeed);
                }

                await MoveDown();

                CurrentSpeed = MaximumSpeed;
            }
            else if (YPos < posY)
            {
                if (YPos + CurrentSpeed > posY)
                {
                    CurrentSpeed = (YPos + CurrentSpeed) - posY;
                }

                await MoveUp();

                CurrentSpeed = MaximumSpeed;
            }
        }

        public async Task MoveUp()
        {
            if (YPos < City.YMax)
            {
                YPos += CurrentSpeed;
                WritePositionToConsole();
                await CallOnMoveAction();
            }
        }

        public async Task MoveDown()
        {
            if (YPos > 0)
            {
                YPos -= CurrentSpeed;
                WritePositionToConsole();
                await CallOnMoveAction();
            }
        }

        public async Task MoveRight()
        {
            if (XPos < City.XMax)
            {
                XPos += CurrentSpeed;
                WritePositionToConsole();
                await CallOnMoveAction();
            }
        }

        public async Task MoveLeft()
        {
            if (XPos > 0)
            {
                XPos -= CurrentSpeed;
                WritePositionToConsole();
                await CallOnMoveAction();
            }
        }

        protected async Task CallOnMoveAction()
        {
            if (Passenger != null)
            {
                await Passenger.OnMoveAction();
            }
        }
    }
}
