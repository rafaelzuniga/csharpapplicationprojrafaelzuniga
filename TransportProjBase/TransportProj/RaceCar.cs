﻿using System;

namespace TransportProj
{
    public class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger)
            : base(xPos, yPos, city, passenger) { }

        public override int MaximumSpeed
        {
            get { return 2; }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine($"Race Car moved to x - {XPos} y - {YPos}");
        }
    }
}
