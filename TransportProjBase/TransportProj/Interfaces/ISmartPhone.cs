﻿using System.Threading.Tasks;

namespace TransportProj.Interfaces
{
    public interface ISmartPhone
    {
        Task VisitWebsite(string url);
    }
}
