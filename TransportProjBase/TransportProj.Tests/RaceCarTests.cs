﻿using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj.Implementations;

namespace TransportProj.Tests
{
    [TestClass]
    public class RaceCarTests
    {
        private City _city;
        private Passenger _passenger;
        private RaceCar _raceCar;

        [TestInitialize]
        public void TestInitialize()
        {
            _city = new City(10, 10);
            _passenger = new Passenger(0, 0, 5, 5, _city, new SmartPhoneMock());
            _raceCar = new RaceCar(5, 5, _city, null);
        }

        [TestMethod]
        public async Task MoveTowardsSlowsDownWhenCloseToTarget()
        {
            await _raceCar.MoveTowards(6, 5);

            Assert.AreEqual(6, _raceCar.XPos);
            Assert.AreEqual(5, _raceCar.YPos);

            await _raceCar.MoveTowards(6, 6);

            Assert.AreEqual(6, _raceCar.XPos);
            Assert.AreEqual(6, _raceCar.YPos);
        }
    }
}
