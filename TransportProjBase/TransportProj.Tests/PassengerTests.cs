﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj.Implementations;

namespace TransportProj.Tests
{
    [TestClass]
    public class PassengerTests
    {
        private City _city;
        private Passenger _passenger;
        private Sedan _sedan;

        [TestInitialize]
        public void TestInitialize()
        {
            _city = new City(10, 10);
            _passenger = new Passenger(5, 5, 10, 10, _city, new SmartPhoneMock());
            _sedan = new Sedan(5, 5, _city, null);
        }

        [TestMethod]
        public void GetInCarAssignsPassengerAndCarCorrectly()
        {
            _passenger.GetInCar(_sedan);

            Assert.AreSame(_passenger, _sedan.Passenger);
            Assert.AreSame(_sedan, _passenger.Car);
        }

        [TestMethod]
        public async Task CurrentPositionMovesWhenCarMoves()
        {
            var initialXPos = _passenger.CurrentXPos;
            var initialYPos = _passenger.CurrentYPos;

            _passenger.GetInCar(_sedan);
            await _sedan.MoveDown();
            await _sedan.MoveLeft();

            Assert.AreEqual(initialXPos - 1, _passenger.CurrentXPos);
            Assert.AreEqual(initialYPos - 1, _passenger.CurrentYPos);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetInCarThrowsWhenPassengerAlreadyInCar()
        {
            _passenger.GetInCar(_sedan);
            _passenger.GetInCar(_sedan);
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void GetOutOfCarThrowsWhenNotInCar()
        {
            _passenger.GetOutOfCar();
        }

        [TestMethod]
        public async Task VisitsWebsiteWhenMovingInCar()
        {
            _passenger.GetInCar(_sedan);
            await _sedan.MoveUp();
            await _sedan.MoveRight();

            Assert.AreEqual(2, _passenger.WebsiteVisitCount);
        }
    }
}
