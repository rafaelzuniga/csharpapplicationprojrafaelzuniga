﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using TransportProj.Implementations;

namespace TransportProj.Tests
{
    [TestClass]
    public class SedanTests
    {
        private City _city;
        private Passenger _passenger;
        private Sedan _sedan;

        [TestInitialize]
        public void TestInitialize()
        {
            _city = new City(10, 10);
            _passenger = new Passenger(0, 0, 5, 5, _city, new SmartPhoneMock());
            _sedan = new Sedan(5, 5, _city, null);
        }

        [TestMethod]
        public void PickupPassengerAssignsPassengerCorrectly()
        {
            _sedan.PickupPassenger(_passenger);

            Assert.AreSame(_passenger, _sedan.Passenger);
        }

        [TestMethod]
        public void HasPassengerIsTrueAfterPickupPassenger()
        {
            _sedan.PickupPassenger(_passenger);

            Assert.IsTrue(_sedan.HasPassenger());
        }

        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void PickupPassengerThrowsWhenPassengerBoardsTwice()
        {
            _sedan.PickupPassenger(_passenger);
            _sedan.PickupPassenger(_passenger);
        }
    }
}
